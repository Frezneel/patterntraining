public class pl1 {
    public static void main(String[] args) {
        String patern = "*";
        int panjang = 5;
        System.out.println("=======BELAJAR LOOPING======\n");
        System.out.println("Nomor 1 : ===============\n");
        //Nomor 1 
        for(int i = 0; i<panjang; i++ ){
            for(int j = 0; j<panjang; j++){
                System.out.print(patern);
            }
            System.out.println("");
        }

        System.out.println("\nNomor 2 : ===============");
        //Nomor 2
        for(int i = 0; i<panjang; i++ ){
            for(int j = 0; j<=i; j++){
                System.out.print(patern);
            }
            System.out.println("");
        }

        System.out.println("\nNomor 3 : ===============");
        //Nomor 3 
        for(int i = 0; i<panjang ;i++){
            for(int j = 5; j>i ; j--){
                System.out.print(patern);
            }
            System.out.println("");
        }

        System.out.println("\nNomor 4 : ===============");
        // Nomor 4
        for(int i = 0; i<panjang ; i++){
            int angka = 1;
            for(int j = 0; j<=i; j++){
                System.out.print(angka+" ");
                angka++;
            }
            System.out.println("");
        }

        System.out.println("\nNomor 5 : ==============="); //Menghemat If
        System.out.println("Menyederhanakan IF");
        for(int i = 0; i<=panjang*2; i++){
            int puncak = i > panjang ? 2*panjang - i : i;
            for(int j = 0; j<puncak; j++){
                System.out.print(patern);}
            System.out.println("");
        }

        System.out.println("\nNomor 5 : ==============="); //pakek if
        System.out.println("PAKAI IF");
        for(int i = 0; i<panjang*2; i++){
           if(i<panjang) {
            for(int j = 0; j<i; j++){
                System.out.print(patern);}
            System.out.println("");
            }
            if (i>=panjang) {
                for(int k = 10; k>i ;k--){
                    System.out.print(patern);
                }
                System.out.println("");
            }
        }

        System.out.println("\nNomor 6 : ==============="); // bingung
        for(int i = 0; i<panjang ; i++){
            for(int j = 0; j<panjang -i ; j++){
                System.out.print(" ");   
            }
            for(int k = 0; k<=i; k++){
                System.out.print(patern);
            }
            System.out.println("");
        }

        System.out.println("\nNomor ? : ==============="); // KETUPAT tangga ISENG DAPET
        for(int i = 0; i<panjang ; i++){
            for(int j = 0; j<=panjang + 2 - (i*2) ; j++){
                System.out.print(" ");   
            }
            for(int k = 0; k<panjang + 1; k++){
                System.out.print(patern);
            }
            System.out.println("");
        }

        System.out.println("\nNomor 7 : ===============");
        // Nomor 7 (Kebalik bisa berabe wkwkw)
        for(int i = 0; i<panjang ;i++){
            for(int k = 0; k<i; k++){
                System.out.print(" ");
            }
            for(int j = 0; j<panjang-i; j++){
                System.out.print(patern);
            }
            
            System.out.println(" ");
        }

        System.out.println("\nNomor 8 : ===============");
        // Nomor 8
        for(int i = 0; i<panjang; i++){
            for(int j=0; j<panjang-(i+1); j++){
                System.out.print(" ");
            }
            for(int k=0; k<(i*2)+1; k++){
                System.out.print(patern);
            }
            System.out.println("");
        }
        
        System.out.println("\nNomor 9 : ================");
        // Nomor 9
        for(int i=0; i<panjang; i++){
            for(int j=0; j<i; j++){
                System.out.print(" ");
            }
            for(int k=0; k<(panjang*2)-(i*2+1); k++){
                System.out.print(patern);
            }
            System.out.println("");
        }

        System.out.println("\nNomor 10 : ================");
        // Nomor 10
        for(int i=0; i<panjang; i++){
            for(int j=0; j<panjang-i-1; j++){
                System.out.print(" ");
            }
            for(int k=0; k<=i; k++){
                System.out.print(patern+" ");
            }
            System.out.println("");
        }

        System.out.println("\nNomor 11 : ================");
        // Nomor 11
        for(int i=0; i<panjang; i++){
            for(int j=0; j<i; j++){
                System.out.print(" ");
            }
            for(int k=0; k<panjang-i; k++){
                System.out.print(patern+" ");
            }
            System.out.println("");
        }

        System.out.println("\nNomor 12 : ================");
        // Nomor 12 (bisa menggunakan math.abs untuk lebih sederhananya)
        for(int i=0; i<panjang*2; i++){
            int puncak = i >= panjang ? (2*panjang)-i-1 : i;
            for(int j=0; j<puncak; j++){
                System.out.print(" ");
            }
            int puncak2 = i < panjang ? panjang-i : (i+1)-(panjang);
            for(int k=0; k<puncak2; k++){
                System.out.print(patern+" ");
            }
            System.out.println("");
        }        

         System.out.println("\nNomor 13 : ================");
        // Nomor 13 ()
        for(int i=0; i<panjang; i++){
            for(int j=0; j<panjang-(i+1); j++){
                System.out.print("0");
            }
            int puncak1 = i < panjang ? (i+1) : panjang; 
            for(int k=0; k<1; k++){
                System.out.print(patern+ " ");
            }
            for(int l=0; l < (i+2)-panjang; l++){
                System.out.print(patern);
            }
            System.out.println();
        }

        // Nomor 13 dari GitHub
        for(int i=0;i<panjang;i++){
            //Print trailing spaces
            for(int j=i;j<panjang-1;j++){
                System.out.print("0");
            }
            //Print hollow pyramid
            for(int j=0;j<=(2*i);j++){
                if(i==4||j==0||j==(2*i)){
                    System.out.print("*");
                }
                else{
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

    }

}