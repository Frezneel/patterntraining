public class Pattern04 {
    public static void resolve(){
        String patern = "*";
        int panjang = 5;
        System.out.println("=======BELAJAR LOOPING======\n");
        System.out.println("\nNomor 4 : ===============");
        // Nomor 4
        for(int i = 0; i<panjang ; i++){
            int angka = 1;
            for(int j = 0; j<=i; j++){
                System.out.print(angka+" ");
                angka++;
            }
            System.out.println("");
        }
    }
}
